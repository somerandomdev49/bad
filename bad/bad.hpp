#pragma once
#include "core.hpp"
using namespace bad;

#define CONCAT_IMPL(x, y) x##y
#define MACRO_CONCAT(x, y) CONCAT_IMPL(x, y)
#define MULTIPLE_IMPL(NAME, ...) static inline auto NAME = [](){ \
  struct { __VA_ARGS__; } x; return x; }; decltype(NAME())

#define circuit static constexpr
#define intermediate auto
#define multiple(...) MULTIPLE_IMPL( \
  MACRO_CONCAT(rettype__, __COUNTER__), __VA_ARGS__)

#define string(N) string<(N)>
#define offset(S, N) offset<(N)>((S))
#define substring(S, ...) substring<__VA_ARGS__>((S))
#define nth(S, N) nth<(N)>((S))
#define on_strings(N, L, F, ...) on_string<(N), (L)>((F), __VA_ARGS__)
#define bit_string(...) bit_string(__VA_ARGS__)
#define concat_strings(...) concat_strings(__VA_ARGS__)
