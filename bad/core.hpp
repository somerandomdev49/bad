#pragma once
#include <cstdint>
#include <cstddef>
#include <concepts>
#include <initializer_list>
#include <array>
#include <bit>
#include <utility>
#include <tuple>

namespace bad {
  struct bit {
    std::uint8_t value;
    constexpr bit() = default;
    constexpr bit(std::uint8_t v) : value(v) {}
    constexpr bit operator and(bit b) { return { value and b.value }; }
    constexpr bit operator or(bit b) { return { value or b.value }; }
    constexpr bit operator xor(bit b) { return { (std::uint8_t)(value xor b.value) }; }
    constexpr bit operator not() { return { !value }; }
  };
  // using bit = std::uint8_t;

  template<int N, bool Zero = false> struct string {
    static_assert(Zero || N != 0, "A bit string cannot be of length 0");
    static_assert(N >= 0, "A bit string cannot be of negative length");
    size_t value; // : (N == 0 ? 1 : N);
  };

  template<int N, int S>
  constexpr string<S - N> offset(string<S> s) {
    static_assert(N < S, "Offset length must be less than the size of the source string.");
    return { (size_t)s.value >> N };
  }

  template<int A, int B = -1, int S>
  constexpr auto substring(string<S> s) -> string<(B < 0 ? S + 1 + B : B) - A> {
    static_assert((B < 0 ? S + 1 + B : B) - A > 0, "Substring range out of bounds.");
    return { (size_t)s.value >> A };
  }

  template<int C, int N>
  constexpr bit nth(string<N> s) {
    static_assert(C < N, "Cannot access bit outside of bit string bounds.");
    return ((size_t)(s.value) >> (C < 0 ? N + C : C)) & 1;
  }

  template<std::convertible_to<bit> ...Ts>
  constexpr string<sizeof...(Ts)> bit_string(Ts ...bits) {
    std::array<bit, sizeof...(Ts)> s = { (bit)bits... };
    size_t r = 0;
    for(int i = sizeof...(Ts); i --> 0;) r |= (size_t)s[i].value << i;
    return { r };
  }

  template<typename T, typename U>
  concept SameAs = std::same_as<std::decay_t<T>, std::decay_t<U>>;
  
  namespace detail_ {
    template<typename... T, std::size_t... I>
    constexpr auto subtuple_(const std::tuple<T...> &t, std::index_sequence<I...>) {
      return std::make_tuple(std::get<I>(t)...);
    }

    template<int N, typename... T>
    constexpr auto subtuple(const std::tuple<T...> &t) {
      return subtuple_(t, std::make_index_sequence<N>());
    }
    
    template<int I, int Start, size_t ...Is>
    constexpr auto nth_bit_tuple_(auto s, std::index_sequence<Is...>) {
      return std::make_tuple(nth<I>(std::get<Start + Is>(s))...);
    }

    template<
      typename Function,
      int L, int StrN, int ArgN,
      size_t ...StrIs,
      size_t ...Is,
      typename ...Args>
    constexpr string<L> on_string(
      Function &&f,
      std::index_sequence<StrIs...>,
      std::index_sequence<Is...>,
      const std::tuple<Args...> &args
    ) {
      // bit_string(
      //   f(nth(A, 0), nth(B, 0), args...),
      //   f(nth(A, 1), nth(B, 1), args...),
      //   f(nth(A, 2), nth(B, 2), args...),
      // );
      
      return bit_string(std::apply(f, std::tuple_cat(
        nth_bit_tuple_<Is, ArgN>(args, std::make_index_sequence<StrN>{}),
        subtuple<ArgN>(args)
      ))...);
    }
  }

  template<int ArgN, int L, typename Function, typename ...Args>
  constexpr string<L> on_string(Function &&f, Args ...args) {
    return detail_::on_string<Function, L, sizeof...(Args) - ArgN, ArgN>(
      std::move(f),
      std::make_index_sequence<sizeof...(Args) - ArgN>{},
      std::make_index_sequence<L>{},
      std::forward_as_tuple(args...)
    );
  }

  namespace detail_ {
    template<int A, int B, size_t ...As, size_t ...Bs>
    constexpr string<A + B> concat_strings(
      string<A> a, string<B> b,
      std::index_sequence<As...>,
      std::index_sequence<Bs...>
    ) {
      return bit_string(nth<As>(a)..., nth<Bs>(b)...);
    }
    
    template<int A, int B>
    constexpr auto operator+(string<A> a, string<B> b) {
      return concat_strings(
        a, b,
        std::make_index_sequence<A>{},
        std::make_index_sequence<B>{}
      );
    }

    template<int A, int B>
    constexpr auto operator+(string<A> a, string<B, true> b) {
      return a;
    }

    template<int A, int B>
    constexpr auto operator+(string<A, true> a, string<B> b) { return b; }
  }

  template<int ...Ns>
  constexpr string<(Ns + ...)> concat_strings(string<Ns> ...as) {
    using namespace detail_;
    return (string<0, true>{} + ... + as);
  }

  template<typename T, int N, typename ...Rest>
  struct GenerateTypeTuple { using type = typename GenerateTypeTuple<T, N - 1, T, Rest...>::type; };

  template<typename T, typename ...Rest>
  struct GenerateTypeTuple<T, 0, Rest...> { using type = std::tuple<Rest...>; };

  template<typename F, int N, typename T, typename R>
  concept TakesArgsOfType = requires(F &&f) {
    { std::apply(std::move(f), typename GenerateTypeTuple<T, N>::type{}) } -> std::same_as<R>;
  };

  template<int N, int M, TakesArgsOfType<M, bit, bit> F>
  constexpr string<1> reduce_string(string<N> s, F &&f);

  namespace detail_ {
    template<int N, int M, int I, size_t ...Is>
    constexpr auto reduce_string_arg_tuple_(string<N> s, std::index_sequence<Is...>) {
      return std::make_tuple(nth<I * M + Is>(s)...);
    }

    template<int N, int M, TakesArgsOfType<M, bit, bit> F, size_t ...Is>
    constexpr string<N / M> reduce_string2_(string<N> s, F &&f, std::index_sequence<Is...>) {
      return bit_string(std::apply(f, reduce_string_arg_tuple_<N, M, Is>(s, std::make_index_sequence<M>{}))...);
    }

    template<int N, int M, TakesArgsOfType<M, bit, bit> F, size_t ...Is>
    constexpr auto reduce_string_(string<N> s, F &&f, std::index_sequence<Is...>) {
      if constexpr((N % M) == 0 && (N / M) == 1) {
        return reduce_string2_<N, M>(s, f, std::make_index_sequence<N / M>{});
      } else if constexpr((N % M) == 0) {
        return reduce_string<N / M, M>(reduce_string2_<N, M>(s, f, std::make_index_sequence<N / M>{}), f);
      } else {
        return reduce_string<N / M + (N % M), M>(concat_strings(
          reduce_string2_<N, M>(s, f, std::make_index_sequence<N / M>{}),
          bit_string(nth<N - (N % M) + Is>(s)...)
        ), f);
      }
    }
  }

  template<int N, int M, TakesArgsOfType<M, bit, bit> F>
  constexpr string<1> reduce_string(string<N> s, F &&f) {
    static_assert(M <= N, "...");
    return detail_::reduce_string_<N, M>(s, f, std::make_index_sequence<N % M>{});
  }
}
