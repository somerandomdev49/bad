#include "bad/bad.hpp"
#include <iostream>

constexpr int log2(size_t val) { return val ? 1 + log2(val >> 1) : -1; }

namespace alu {
  multiple(bit sum; bit carry)
  circuit half_adder(bit A, bit B) {
    return {
      .sum = (bit)(A xor B),
      .carry = A and B
    };
  }

  template<int N>
  struct adder;
  
  template<>
  struct adder<1> {
    multiple(string(1) sum; bit carry)
    operator()(string(1) A, string(1) B, bit C) {
      intermediate t1 = half_adder(nth(A, 0), nth(B, 0));
      intermediate t2 = half_adder(C, t1.sum);

      return {
        .sum = bit_string(t2.sum),
        .carry = t1.carry or t2.carry
      };
    }
  };

  template<int N>
  struct adder {
    multiple(string(N) sum; bit carry)
    operator()(string(N) A, string(N) B, bit C) {
      intermediate t1 = adder<1 << (log2(N) - 1)>()(
        substring(A, 0, 1 << (log2(N) - 1)),
        substring(B, 0, 1 << (log2(N) - 1)),
        C
      );

      intermediate t2 = adder<N - (1 << (log2(N) - 1))>()(
        substring(A, 1 << (log2(N) - 1)),
        substring(B, 1 << (log2(N) - 1)),
        t1.carry
      );
      
      return {
        .sum = concat_strings(t1.sum, t2.sum),
        .carry = t2.carry
      };
    }
  };
}

// this is not fully modeled, i'm too lazy to do that.
namespace mmu {
  string(8) MEMORY[0xFFF] = { 0 };

  string(8) circuit get(string(12) address) {
    return MEMORY[address.value];
  }

  void circuit set(string(12) address, string(8) value) {
    MEMORY[address.value] = value;
  }
}

namespace cpu {

  template<int N, int M, SameAs<string(N)> ...Args>
  string(N) circuit multiplexer(string(M) C, Args ...args);

  bit circuit multiplexer(bit A, bit B, bit C) {
    return ((not C) and A) or (C and B);
  }

  template<
    int N, int M,
    SameAs<string(N)> ...Args,
    size_t ...Is>
  string(N) circuit multiplexer_(
    string(M) C,
    const std::tuple<Args...> &args,
    std::index_sequence<Is...>
  ) {
    if constexpr(M == 1) {
      return on_strings(0, N, [](bit a, bit b){ return a or b; },
        on_strings(
          1, N, [](bit a, bit b){ return a and (not b); },
          nth(C, 0),
          std::get<0>(args)
        ),
        on_strings(
          1, N, [](bit a, bit b){ return a and b; },
          nth(C, 0),
          std::get<1>(args)
        )
      );
    } else {
      return multiplexer<N, 1>(
        bit_string(nth(C, -1)),
        multiplexer<N, M - 1>(
          substring(C, 0, -2),
          std::get<Is>(args)...
        ),
        multiplexer<N, M - 1>(
          substring(C, 0, -2),
          std::get<Is + (1 << (M - 1))>(args)...
        )
      );
    }
  }

  template<int N, int M, SameAs<string(N)> ...Args>
  string(N) circuit multiplexer(string(M) C, Args ...args) {
    static_assert(sizeof...(Args) == 1 << M, "Wrong number of inputs for multiplexer.");
    return multiplexer_<N, M>(C, std::forward_as_tuple(args...),
      std::make_index_sequence<1 << (M - 1)>{});
  }

  void circuit tick() {
    // auto x = multiplexer<8, 2>(string(2){2}, string(8){12}, string(8){2}, string(8){3}, string(8){4});
    auto x = reduce_string<5, 2>(bit_string(1, 1, 1, 1, 1), [](bit a, bit b) -> bit { return a and b; });
    std::cout << x.value << std::endl;
  }
}

int main() {
  cpu::tick();
}
