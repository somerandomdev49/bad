# C++ Boolean Algebra DSL

> The name: **B**oolean **A**lgebra **D**SL

This is a WIP DSL in C++. The whole of the library code is currently in [`bad/bad.hpp`](/bad/bad.hpp) and [`bad/core.hpp`](/bad/core.hpp). An example of how to use the DSL is in [`example.cpp`](/example.cpp)

## Running the example

For this DSL to work, you need a C++20 compiler. For now you simply need to compile the `example.cpp` file:

```bash
$ clang++ -std=c++20 example.cpp -o program
```

## Code

* [`bad/bad.hpp`](/bad/bad.hpp) contains the macros needed for the DSL to work.
* [`bad/core.hpp`](/bad/core.hpp) contains the logic behind all of the macros.

I'm too lazy to write the documentation, but you can have a look in [`example.cpp`](/example.cpp) for reference.
<sub>eventually I will write the documentation of course...</sub>

#### I see `if`s and confusing logic everywhere, how do I know if it's actually computing everything through boolean algebra?

If you take a closer look, every `if` is an `if constexpr` and every calculation apart from bit manipulations
is done at *compile* time (either through templates or `constexpr` functions)! All other logic can be simplified
into a boolean algebra expression or a circuit of logic gates.

## Roadmap

* create a system of custom backends (for example, instead of computing the actual values, you get a circuit in return)
* create a standard library (derived from `example.cpp` probably)
* that is all for now, if you have any ideas, feel free to make an issue :)
